import apiService, { API_ROOT } from "./apiService";

class KinooApi {
  getIAllMovies() {
    return apiService.get(`${API_ROOT}/movie`);
  }

  getMovie(movieTitle: string) {
    return apiService.get(`${API_ROOT}/movie/${movieTitle}`);
  }


  rate(id: string, rate: number) {
    return apiService.post(`${API_ROOT}/rate/${id}`, { rate });
  }

}

const kinooApi = new KinooApi();

export default kinooApi;
