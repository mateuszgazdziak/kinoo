import React from "react";
import NavBar from "./components/NavBar/NavBar";
import MovieList from "./components/MovieList/MovieList";
import MovieView from "./components/MovieView/MovieView";

import "./App.css";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";

const App: React.FC = () => {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route exact path="/" component={MovieList} />
        <Route exact path="/movie/:id" component={MovieView} />
      </Switch>
    </Router>
  );
};

export default App;
