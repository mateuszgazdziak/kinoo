import React from "react";

type Props = {
  fill?: string;
  width?: string;
  height?: string;
  className?: string;
  viewBox?: string;
};

const defaultProps: Props = {
  width: "24",
  height: "24",
  viewBox: "0 0 24 24",
  fill: "none"
};

const SVG: React.FC<Props> = props => {
  const { width, height, viewBox, fill } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewBox}
      fill={fill}
      stroke="#e7e8e5"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <circle cx="12" cy="5" r="3" />
      <line x1="12" y1="22" x2="12" y2="8" />
      <path d="M5 12H2a10 10 0 0 0 20 0h-3" />
    </svg>
  );
};

SVG.defaultProps = defaultProps;

export default SVG;
