import React from "react";

import Action from "./Action";
import Adventure from "./Adventure";
import Comedy from "./Comedy";
import Drama from "./Drama";

type Props = {
  name: string;
  fill?: string;
  width?: string;
  height?: string;
  className?: string;
  viewBox?: string;
};

const Icon: React.FC<Props> = props => {
  switch (props.name) {
    case "action":
      return <Action {...props} />;
    case "adventure":
      return <Adventure {...props} />;
    case "comedy":
      return <Comedy {...props} />;
    case "drama":
      return <Drama {...props} />;
    default:
      return <Action {...props} />;
  }
};

export default Icon;
