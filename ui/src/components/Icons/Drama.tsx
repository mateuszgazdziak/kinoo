import React from "react";

type Props = {
  fill?: string;
  width?: string;
  height?: string;
  className?: string;
  viewBox?: string;
};

const defaultProps: Props = {
  width: "24",
  height: "24",
  viewBox: "0 0 24 24",
  fill: "none"
};

const SVG: React.FC<Props> = props => {
  const { width, height, viewBox, fill } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox={viewBox}
      fill={fill}
      stroke="#e7e8e5"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M12 2.69l5.66 5.66a8 8 0 1 1-11.31 0z" />
    </svg>
  );
};

SVG.defaultProps = defaultProps;

export default SVG;
