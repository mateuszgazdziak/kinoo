import React, { useState, useEffect } from "react";
import kinooApi from "./../../utils/api/kinooApi";
import "./MovieList.css";
import MovieListItem from "../MovieListItem/MovieListItem";

const MovieList: React.FC = () => {
  const [movies, setMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const result = await kinooApi.getIAllMovies();

        setMovies(result);
      } catch (error) {
        setIsError(true);
      }

      setIsLoading(false);
    };

    fetchData();
  }, []);

  const renderListOfMovies = () =>
    movies.map((movie: any) => {
      return (
        <MovieListItem
          key={`${movie.title}_${movie.genre}`}
          id={movie.id}
          title={movie.title}
          averageRate={movie.averageRate || "N/A"}
          genre={movie.genre}
        />
      );
    });

  const renderContent = () => {
    if (isError) {
      return <h2>Something went wrong ...</h2>;
    } else if (isLoading) {
      return <h2>Loading data...</h2>;
    } else if (!movies.length) {
      return <h2>There is no movies in database</h2>;
    } else {
      return <ul>{renderListOfMovies()}</ul>;
    }
  };

  return (
    <div className="movieListContainer">
      <h2>List of Movies</h2>
      {renderContent()}
    </div>
  );
};

export default MovieList;
