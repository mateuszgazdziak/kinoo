import React, { useState, useEffect } from "react";
import "./MovieView.css";
import Icon from "../Icons";
import kinooApi from "./../../utils/api/kinooApi";
import "./MovieView.css";

import { withRouter, RouteComponentProps } from "react-router-dom";
import Dropdown from "./../Dropdown/Dropdown";

type QueryParams = {
  id: string;
};

const MovieView: React.FC<RouteComponentProps<QueryParams>> = props => {
  const [rate, setRate] = useState(5);
  const [title, setTitle] = useState("");
  const [genre, setGenre] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const id = props.match.params.id;

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setIsError(false);

      try {
        const result = await kinooApi.getMovie(id);

        setTitle(result.title);
        setGenre(result.genre);
      } catch (error) {
        setIsError(true);
      }

      setIsLoading(false);
    };

    fetchData();
  }, []);

  const renderContent = () => {
    if (isError) {
      return <h2>Something went wrong ...</h2>;
    } else if (isLoading) {
      return <h2>Loading data...</h2>;
    } else {
      return (
        <>
          {renderMovieDetails()}
          {renderButton()}
        </>
      );
    }
  };

  const handleOnSelect = (rate: number) => {
    setRate(rate);
  }

  const handleOnRate = async () => {
    try {
      await kinooApi.rate(id, +rate);
      props.history.push("/")

    } catch (error) {
      setIsError(true);
    }

  };

  const renderMovieDetails = () => (
    <div className="movieContainer">
      <h3>{title}</h3>
      <>
        <span>
          Genre: <Icon name={genre.toLocaleLowerCase()} /> {genre}
        </span>
        <br />
      </>
      <span>Salect rate: <Dropdown handleOnSelect={handleOnSelect}/></span>
    </div>
  );

  const renderButton = () => {
    return <button onClick={handleOnRate}>Rate</button>;
  };
  return <>{renderContent()}</>;
};

export default withRouter(MovieView);
