import React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import "./NavBar.css";

const NavBar: React.FC<RouteComponentProps> = props => {
  const handleHomeButtonClick = () => {
    props.history.push("/");
  };

  return (
    <nav>
      <div className="navWide">
        <div className="wideDiv">
          <a className="homeButton" onClick={handleHomeButtonClick}>
            kinoo
          </a>
        </div>
      </div>
    </nav>
  );
};

export default withRouter(NavBar);
