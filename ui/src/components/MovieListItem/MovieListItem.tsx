import React from "react";
import Icon from "../Icons";
import { withRouter, RouteComponentProps } from "react-router-dom";
interface Props extends RouteComponentProps {
  id: Number;
  title: String;
  genre: String;
  averageRate: Number | String;
}

const MovieListItem: React.FC<Props> = props => {
  const { id, title, genre, averageRate, history } = props;

  const handleOnClick = () => {
    history.push(`/movie/${id}`);
  };

  return (
    <li>
      <a onClick={handleOnClick}>
        <h3>{title}</h3>
      </a>
      <>
        <span>
          Genre: <Icon name={genre.toLocaleLowerCase()} /> {genre}
        </span>
        <br />
      </>
      Average rate: {averageRate}
    </li>
  );
};

export default withRouter(MovieListItem);
