import React, { useState } from "react";

type Props = {
  handleOnSelect: (value: number) => void;
};

const Dropdown: React.FC<Props> = props => {
  const [value, setValue] = useState(5);

  const handleOnSelect = event => {
    setValue(event.target.value)
    props.handleOnSelect(event.target.value);
  };

  const renderOptions = () => {
    const options = [];
    for (let i = 1; i <= 10; i++) {
      options.push(<option key={i} value={i}>{i}</option>);
    }
    return options;
  };

  return (
    <select onChange={handleOnSelect} value={value}>
      {renderOptions()}
    </select>
  );
};

export default Dropdown;
