import React from "react";
import ReactDOM from "react-dom";
import MovieListItem from "./../MovieListItem/MovieListItem";
import { BrowserRouter } from "react-router-dom";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <BrowserRouter>
      <MovieListItem averageRate={4.44} title="test" genre="genre" id={3} />
    </BrowserRouter>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
