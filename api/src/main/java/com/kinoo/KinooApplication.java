package com.kinoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KinooApplication {

	public static void main(String[] args) {
		SpringApplication.run(KinooApplication.class, args);
	}

}
