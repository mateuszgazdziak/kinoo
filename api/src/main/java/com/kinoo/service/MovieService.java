package com.kinoo.service;

import com.kinoo.exception.GenreNameException;
import com.kinoo.exception.MovieTitleException;
import com.kinoo.model.Genre;
import com.kinoo.model.Movie;
import com.kinoo.model.Rating;
import com.kinoo.repository.GenreRepository;
import com.kinoo.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieService {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private MovieRepository movieRepository;

    public Movie addMovie(String genre, Movie movie) {
        Genre movieGenre = genreRepository.findByName(genre);

        if (movieGenre == null) {
            throw new GenreNameException("Genre name: " + genre + " does not exist");
        }
        movie.setGenre(movieGenre);
        return movieRepository.save(movie);
    }

    public Iterable<Map<String, Object>> findAllMovies() {
        List<Map<String, Object>> moviesResponse = new ArrayList<>();
        Iterable<Movie> movies = movieRepository.findAll();

        movies.forEach(movie -> moviesResponse.add(this.mapMovieResponse(movie)));

        return moviesResponse;
    }

    public Map<String, Object> findMovieById(Long id) {
        Movie movie =  movieRepository.findById(id).orElseThrow(() -> new MovieTitleException("Movie with id: " + id + " does not exist"));
        return this.mapMovieResponse(movie);
    }

    public void deleteMovieByTitle(String title) {
        Movie movie = movieRepository.findByTitle(title);

        if (movie == null) {
            throw new MovieTitleException("Movie with title: " + title + " does not exist");
        }
        movieRepository.delete(movie);
    }

    private Map<String, Object> mapMovieResponse(Movie movie) {
        Map<String, Object> movieResponse = new HashMap<>();
        movieResponse.put("id", movie.getId());
        movieResponse.put("title", movie.getTitle());
        movieResponse.put("genre", movie.getGenre().getName());
        movieResponse.put("averageRate", this.getAverageRating(movie.getRatings()));

        return movieResponse;
    }

    public Double getAverageRating(List<Rating> ratings) {

        if (ratings.size() == 0) {
            return null;
        }

        Double sum = (double) ratings.stream().map(Rating::getRate).reduce(0, (a, b) -> a + b);
        Double average = sum / ratings.size();
        return Math.round(average * 100.0) / 100.0;

    }

}
