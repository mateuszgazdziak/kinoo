package com.kinoo.service;

import com.kinoo.exception.MovieTitleException;
import com.kinoo.model.Movie;
import com.kinoo.model.Rating;
import com.kinoo.repository.MovieRepository;
import com.kinoo.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingService {

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private MovieRepository movieRepository;

    public Rating addRating(Long id, Rating rating) {
        Movie movie = movieRepository.findById(id).orElseThrow(() -> new MovieTitleException("Movie with id: " + id + " does not exist"));
        rating.setMovie(movie);

        return ratingRepository.save(rating);


    }


}
