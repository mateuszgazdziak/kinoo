package com.kinoo.service;

import com.kinoo.exception.GenreNameException;
import com.kinoo.model.Genre;
import com.kinoo.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    public Genre saveOrUpdate(Genre genre) {
        try {
            return genreRepository.save(genre);
        } catch (Exception e) {
            throw new GenreNameException("Genre name: " + genre.getName() + " is not unique");
        }

    }

    public Genre findGenreByName(String name) {
        Genre genre = genreRepository.findByName(name);

        if (genre == null) {
            throw new GenreNameException("Genre name: " + name + " does not exist");
        }

        return genre;
    }

    public Iterable<Genre> findAllGenres() {
        return genreRepository.findAll();
    }

    public void deleteGenreByName(String name) {
        Genre genre = genreRepository.findByName(name);

        if (genre == null) {
            throw new GenreNameException("Genre name: " + name + " does not exist");
        }

        genreRepository.delete(genre);
    }
}
