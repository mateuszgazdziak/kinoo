package com.kinoo.exception;

public class GenreNameExceptionResponse {
    private String name;

    public GenreNameExceptionResponse(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
