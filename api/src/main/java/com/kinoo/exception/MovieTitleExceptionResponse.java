package com.kinoo.exception;

public class MovieTitleExceptionResponse {

    private String title;

    public MovieTitleExceptionResponse(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
