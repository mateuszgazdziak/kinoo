package com.kinoo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class GenreNameException extends RuntimeException {
    public GenreNameException(String message) {
        super(message);
    }
}
