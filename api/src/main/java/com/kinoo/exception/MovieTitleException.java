package com.kinoo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MovieTitleException extends RuntimeException {
    public MovieTitleException(String message) {
        super(message);
    }
}
