package com.kinoo.api;

import com.kinoo.model.Movie;
import com.kinoo.service.MapErrorService;
import com.kinoo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RequestMapping("api/v0/movie")
@RestController
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MapErrorService mapErrorService;

    @PostMapping("/{genre}")
    public ResponseEntity<?> addNewMovie(@Valid @RequestBody Movie movie, BindingResult result, @PathVariable String genre) {

        ResponseEntity<?> errorMap = mapErrorService.MapValidationError(result);

        if (errorMap != null) {
            return errorMap;
        }

        Movie newMovie = movieService.addMovie(genre, movie);

        return new ResponseEntity<Movie>(newMovie, HttpStatus.CREATED);

    }

    @GetMapping()
    public Iterable<Map<String, Object>> getAllMovies() {
        return movieService.findAllMovies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMovieByTitle(@PathVariable Long id) {
        Map<String, Object> movie = movieService.findMovieById(id);
        return new ResponseEntity<Map<String, Object>>(movie, HttpStatus.OK);
    }

    @DeleteMapping("/title")
    public ResponseEntity<?> deleteMovie(@PathVariable String title) {
        movieService.deleteMovieByTitle(title);
        return new ResponseEntity<String>("Movie with title: " + title + " was deleted", HttpStatus.OK);
    }
}
