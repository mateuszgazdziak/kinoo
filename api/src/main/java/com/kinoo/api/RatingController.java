package com.kinoo.api;

import com.kinoo.model.Rating;
import com.kinoo.service.MapErrorService;
import com.kinoo.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v0/rate")
public class RatingController {

    @Autowired
    private RatingService ratingService;

    @Autowired
    private MapErrorService mapErrorService;

    @PostMapping("/{id}")
    public ResponseEntity<?> rateMovie(@Valid @RequestBody Rating rating, BindingResult result, @PathVariable Long id) {

        ResponseEntity<?> errorMap = mapErrorService.MapValidationError(result);
        if (errorMap != null) return errorMap;

        Rating newRating = ratingService.addRating(id, rating);

        return new ResponseEntity<Rating>(newRating, HttpStatus.CREATED);
    }
}