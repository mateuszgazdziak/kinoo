package com.kinoo.api;

import com.kinoo.model.Genre;
import com.kinoo.service.GenreService;
import com.kinoo.service.MapErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api/v0/genres")
@RestController
public class GenreController {


    @Autowired
    private GenreService genreService;

    @Autowired
    private MapErrorService mapErrorService;

    @PostMapping("")
    public ResponseEntity<?> addNewGenre(@Valid @RequestBody Genre genre, BindingResult result) {

        ResponseEntity<?> errorMap = mapErrorService.MapValidationError(result);

        if (errorMap != null) {
            return errorMap;
        }

        Genre newGenre = genreService.saveOrUpdate(genre);
        return new ResponseEntity<Genre>(newGenre, HttpStatus.CREATED);
    }

    @GetMapping("/{name}")
    public ResponseEntity<?> getGenreByName(@PathVariable String name) {
        Genre genre = genreService.findGenreByName(name);
        return new ResponseEntity<Genre>(genre, HttpStatus.OK);
    }

    @GetMapping()
    public Iterable<Genre> getAllGenres() {
        return genreService.findAllGenres();
    }

    @DeleteMapping("/{name}")
    public ResponseEntity<?> deleteGenre(@PathVariable String name) {
        genreService.deleteGenreByName(name);
        return new ResponseEntity<String>("Genre with name: " + name + " was deleted" , HttpStatus.OK);
    }
}
