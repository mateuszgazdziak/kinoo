package com.kinoo.repository;

import com.kinoo.model.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> {

    Movie findByTitle(String title);

    @Override
    Iterable<Movie> findAll();

}
