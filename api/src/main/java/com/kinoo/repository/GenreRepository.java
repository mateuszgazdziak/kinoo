package com.kinoo.repository;

import com.kinoo.model.Genre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GenreRepository extends CrudRepository<Genre, Long> {

    Genre findByName(String name);

    @Override
    Iterable<Genre> findAll();
}
