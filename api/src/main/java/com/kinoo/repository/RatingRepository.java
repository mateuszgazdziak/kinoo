package com.kinoo.repository;

import com.kinoo.model.Rating;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends CrudRepository<Rating, Long> {

    @Override
    Iterable<Rating> findAll();
}
