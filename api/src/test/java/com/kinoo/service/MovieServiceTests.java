package com.kinoo.service;

import com.kinoo.model.Rating;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MovieServiceTests {

    @Test
    public void testGetAverageRatingWithEmptyList() {
        MovieService movieService = new MovieService();

        Double resultWithEmptyList = movieService.getAverageRating(new ArrayList<Rating>());
    }

    @Test
    public void testGetAverageRating() {
        MovieService movieService = new MovieService();

        List<Rating> listOfRatings = new ArrayList<>();
        Rating rating1 = new Rating();
        rating1.setRate(3);

        Rating rating2 = new Rating();
        rating2.setRate(2);

        Rating rating3 = new Rating();
        rating3.setRate(6);

        Rating rating4 = new Rating();
        rating4.setRate(5);

        listOfRatings.add(rating1);
        listOfRatings.add(rating2);
        listOfRatings.add(rating3);
        listOfRatings.add(rating4);

        Double result = movieService.getAverageRating(listOfRatings);
        assertEquals(4, result, 0);

    }
}
