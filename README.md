# kinoo

To run loccally:
1. Package api:

``cd ./api && mvn package -DskipTests``

``cd ./docker/ && docker-compose up -d``

Deployed on AWS:
http://ec2-3-16-112-144.us-east-2.compute.amazonaws.com:8000/ 

## backend

 - Spring Boot 2
 - Hibernate
 - postgres

## frontend

 - typescript
 - hooks
